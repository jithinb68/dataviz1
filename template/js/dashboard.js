(function($) {
  'use strict';

if ($("#customerschart").length) {
    var customerschartcanvas = document.getElementById('customerschart');
    var customerschart = new Chart(customerschartcanvas, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        datasets: [{
            data: [20000,40000,20000,40000,60000,40000,60000,85000,92556,60000,25000,20000],
            backgroundColor: "#2c50ed"
        }]
    },
    options: {
        responsive: true,
        legend: {
            display: false
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex flex-column justify-content-center align-items-center">');
            text.push('<h1 class="font-weight-bold">'+ Math.max(...chart.data.datasets[0].data)+'</h1>');
            text.push('<div class="d-flex justify-content-center align-items-baseline flex-wrap">');
            text.push('<div class="d-flex align-items-baseline">');
            text.push('<h6 class="text-success">1.35</h6>');
            text.push('<i class="mdi mdi-arrow-up text-success"></i>');
            text.push('</div>');
            text.push('<h6 class="ml-lg-1 text-muted">More than last month</h6>');
            text.push('</div>');
            text.push('</div>')
            return text.join('');
        },
        scales: {
            yAxes: [{
                display: false,
                ticks: {
                    display: false,
                    min: 0
                }       
            }],
            xAxes: [{
                display: false,
                ticks: {
                    display: false,
                }      
            }],
        }
    }
});
document.querySelector('#customerschart-legend').innerHTML = customerschart.generateLegend();
}

if ($("#revenuechart").length) {
var revenuechartcanvas = document.getElementById('revenuechart');
var revenuechart = new Chart(revenuechartcanvas, {
    type: 'line',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
        datasets: [{
            data: [20000,30000,40000,50000,56556,40000,30000,30000,40000,50000,50000,40000,30000,20000],
            borderColor: "rgba(251, 188, 6, 0.73)",
            borderWidth: 0,
            backgroundColor: 'rgba(251, 188, 6,0.73)',
            pointRadius: 0
        }, {
            data: [60000,50000,40000,30000,30000,40000,50000,60000,60000,50000,40000,50000,60000,70000],
            borderColor: "rgba(251, 188, 6, 0.73)",
            borderWidth: 0,
            backgroundColor: 'rgba(251, 188, 6, 0.73)',
            pointRadius: 0
        }]
    },
    options: {
        scales: {
            xAxes: [{
                display: false,
                gridLines: {
                    display: false,
                },
                ticks: {
                }
            }],
            yAxes: [{
                display: false,
                gridLines: {
                    drawBorder: false
                },
                ticks: {
                    min: 0,
                    max: 80000,
                    stepSize: 10000
                }
            }]
        },
        legend: {
            display: false
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex flex-column justify-content-center align-items-center">');
            text.push('<h1 class="font-weight-bold">'+ Math.max(...chart.data.datasets[0].data)+'</h1>');
            text.push('<div class="d-flex justify-content-center align-items-baseline flex-wrap">');
            text.push('<h6 class="text-success">0.06</h6>');
            text.push('<i class="mdi mdi-arrow-up text-success"></i>');
            text.push('<h6 class="ml-1 text-muted">More than last month</h6>');
            text.push('</div>');
            text.push('</div>');
            return text.join('');
        },
        tooltips: {
            enabled: false
        }
    }
});
document.querySelector('#revenuechart-legend').innerHTML = revenuechart.generateLegend();
}

if ($("#conversionchart").length) {
var conversionchartcanvas = document.getElementById('conversionchart');
var conversionchart = new Chart(conversionchartcanvas, {
    type: 'line',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10],
        datasets: [{
            data: [64545,55454,34343,43433,33434,63232,54343,65644,74343,63232],
            backgroundColor: "#f5dde3",
            pointRadius: 0,
            borderColor: "#ff3366"
        }]
    },
    options: {
        responsive: true,
        scales: {
            xAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    display: false
                }              
            }],
            yAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    display: false,
                    min: 0,
                    max: 80000,
                    stepSize: 10000
                }
            }]
        },
        legend: {
            display: false
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex flex-column justify-content-center align-items-center">');
            text.push('<h1 class="font-weight-bold">'+ Math.max(...chart.data.datasets[0].data)+'</h1>');
            text.push('<div class="d-flex justify-content-center align-items-baseline flex-wrap">');
            text.push('<h6 class="text-danger">0.17</h6>');
            text.push('<i class="mdi mdi-arrow-down text-danger"></i>');
            text.push('<h6 class="ml-1 text-muted">More than last month</h6>');
            text.push('</div>');
            text.push('</div>');
            return text.join('');
        }
    }
});
document.querySelector('#conversionchart-legend').innerHTML = conversionchart.generateLegend();
}

if ($("#conversionchart-dark").length) {
    var conversionchartcanvas = document.getElementById('conversionchart-dark');
    var conversionchart = new Chart(conversionchartcanvas, {
        type: 'line',
        data: {
            labels: [1,2,3,4,5,6,7,8,9,10],
            datasets: [{
                data: [64545,55454,34343,43433,33434,63232,54343,65644,74343,63232],
                backgroundColor: "#ffb3c6",
                pointRadius: 0,
                borderColor: "#ff3366"
            }]
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        display: false
                    }              
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        display: false,
                        min: 0,
                        max: 80000,
                        stepSize: 10000
                    }
                }]
            },
            legend: {
                display: false
            },
            legendCallback : function(chart) {
                var text = [];
                text.push('<div class="d-flex flex-column justify-content-center align-items-center">');
                text.push('<h1 class="font-weight-bold">'+ Math.max(...chart.data.datasets[0].data)+'</h1>');
                text.push('<div class="d-flex justify-content-center align-items-baseline flex-wrap">');
                text.push('<h6 class="text-danger">0.17</h6>');
                text.push('<i class="mdi mdi-arrow-down text-danger"></i>');
                text.push('<h6 class="ml-1 text-muted">More than last month</h6>');
                text.push('</div>');
                text.push('</div>');
                return text.join('');
            }
        }
    });
    document.querySelector('#conversionchart-legend').innerHTML = conversionchart.generateLegend();
    }

if ($("#revenuelastdays").length) {
var revenuelastdayscanvas = document.getElementById("revenuelastdays");
var graphGradient = document.getElementById("revenuelastdays").getContext('2d');
var saleGradientBg = graphGradient.createLinearGradient(0, 0, 0, 400);
saleGradientBg.addColorStop(0, 'rgba(5, 241, 186, 0.2)');
saleGradientBg.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
var graphGradient = document.getElementById("revenuelastdays").getContext('2d');
var saleGradientBg1 = graphGradient.createLinearGradient(0, 0, 0, 450);
saleGradientBg1.addColorStop(0, '#dcd7ff');
saleGradientBg1.addColorStop(1, 'rgba(217, 217, 217, 0)');
var revenuelastdays = new Chart(revenuelastdayscanvas, {
    type: "line",
    data:{
        labels: ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'],
        datasets: [{
            data: [600,620,625,550,620,630,560,600,730,700,690,780],
            borderColor: "#00dac5",
            backgroundColor: saleGradientBg,
            pointRadius: 0
            },
            {
            data: [605,800,700,650,800,850,780,820,1100,850,800,900],
            borderColor: "#6201ed",
            backgroundColor: saleGradientBg1,
            pointRadius: 0
            }
        ]
    },
    options: {
        legend: {
            display: false
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex align-items-center">');
            text.push('<div class="text-small-13px">Online revenue</div>');
            text.push('<div class="ml-3" style="width: 12px; height: 12px; background-color: ' + chart.data.datasets[1].borderColor + ' "></div>');
            text.push('</div>');
            return text.join('');
          },
        scales: {
            xAxes: [{
                ticks: {
                    padding: 8,
                    fontColor: "#979797",
                    fontSize: 11
                },
                gridLines: {
                    display: false,
                    drawBorder: false,
                    color: "#f1f3f9"
                }
            }],
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    color: "#f1f3f9"
                },
                ticks: {
                min: 200,
                max: 1200,
                stepSize: 200,
                padding: 8,
                fontColor: "#979797",
                fontSize: 11
                }
            }]
        }
    }    
});
document.querySelector('#revenuelastdays-legend').innerHTML = revenuelastdays.generateLegend();
}

if ($("#revenuelastdays-dark").length) {
    var revenuelastdayscanvas = document.getElementById("revenuelastdays-dark");
    var graphGradient = document.getElementById("revenuelastdays-dark").getContext('2d');
    var saleGradientBg = graphGradient.createLinearGradient(0, 0, 0, 400);
    saleGradientBg.addColorStop(0, 'rgba(0, 218, 197,0.8)');
    saleGradientBg.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
    var graphGradient = document.getElementById("revenuelastdays-dark").getContext('2d');
    var saleGradientBg1 = graphGradient.createLinearGradient(0, 0, 0, 450);
    saleGradientBg1.addColorStop(0, 'rgba(98, 1, 237,0.8)');
    saleGradientBg1.addColorStop(1, 'rgba(217, 217, 217, 0)');
    var revenuelastdays = new Chart(revenuelastdayscanvas, {
        type: "line",
        data:{
            labels: ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'],
            datasets: [{
                data: [600,620,625,550,620,630,560,600,730,700,690,780],
                borderColor: "#00dac5",
                backgroundColor: saleGradientBg,
                pointRadius: 0
                },
                {
                data: [605,800,700,650,800,850,780,820,1100,850,800,900],
                borderColor: "#6201ed",
                backgroundColor: saleGradientBg1,
                pointRadius: 0
                }
            ]
        },
        options: {
            legend: {
                display: false
            },
            legendCallback : function(chart) {
                var text = [];
                text.push('<div class="d-flex align-items-center">');
                text.push('<div class="text-small-13px">Online revenue</div>');
                text.push('<div class="ml-3" style="width: 12px; height: 12px; background-color: ' + chart.data.datasets[1].borderColor + ' "></div>');
                text.push('</div>');
                return text.join('');
              },
            scales: {
                xAxes: [{
                    ticks: {
                        padding: 8,
                        fontColor: "#d7d8db",
                        fontSize: 11
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false,
                        color: "rgba(47, 50, 78, 0.4)"
                    }
                }],
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        color: "rgba(47, 50, 78, 0.4)"
                    },
                    ticks: {
                        min: 200,
                        max: 1200,
                        stepSize: 200,
                        padding: 8,
                        fontColor: "#d7d8db",
                        fontSize: 11
                    }
                }]
            }
        }    
    });
    document.querySelector('#revenuelastdays-legend').innerHTML = revenuelastdays.generateLegend();
    }



if ($("#doughnutchart").length) {
var doughnutchartcanvas = document.getElementById("doughnutchart");
var doughnutchart = new Chart(doughnutchartcanvas, {
    type : "doughnut",
    data: {
        labels: ["Blue","Green","Red"],
        datasets: [{
            data: [32,40,28],
            backgroundColor : [ 
            '#6201ed',
            '#1ad5c3',
            "#2c50ed"
        ],
        borderWidth: 0
            }]
        },
    options: {
        responsive: true,
        cutoutPercentage: 60,
        legend : {
            display: false,
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex justify-content-between align-items-center px-2">');
            text.push('<div class="d-flex justify-content-between align-items-center flex-column">');
            text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[0].data[0] +'%</h4>');
            text.push('<p>Sales total</p>');
            text.push('</div>');
            text.push('<div class="d-flex justify-content-between align-items-center flex-column">');
            text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[0].data[1] +'%</h4>');
            text.push('<p>Sales total</p>');
            text.push('</div>');
            text.push('<div class="d-flex justify-content-between align-items-center flex-column">');
            text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[0].data[2] +'%</h4>');
            text.push('<p>Sales total</p>');
            text.push('</div>');
            text.push('</div>');
            return text.join('');
        }
    }
});
document.querySelector('#doughnutchart-legend').innerHTML = doughnutchart.generateLegend();
}

if ($("#saleschart").length) {
var saleschartcanvas = document.getElementById("saleschart");
var saleschart = new Chart(saleschartcanvas, {
    type: "line",
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11,12,13],
        datasets: [{
            data: [10,5,8,6,3,5,7,8,3,6,5,8,4,6,7],
            backgroundColor: "#1ad5c3",
            pointRadius: 0,
            borderWidth: 0
            },
            {
            data: [12,14,15,13,17,19,16,14,13,12,16,17,14,12,14,16,18],
            backgroundColor: "#e3e3e3",
            pointRadius: 0,
            borderWidth: 0
            }
        ]
    },
    options: {
        legend: {
            display: false,
        },
        legendCallback : function(chart) {
            var text = [];
            text.push('<div class="d-flex align-items-center">');
            text.push('<div class="d-flex flex-column">');
            text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[0].data[0]+ '</h4 >');
            text.push('<div style="width: 24px; height: 12px; background-color: ' + chart.data.datasets[0].backgroundColor + ' "></div>');
            text.push('<p class="pt-2 m-0">Sales</p>')
            text.push('</div>');
            text.push('<div class="d-flex flex-column pl-4">');
            text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[1].data[0]+ '</h4 >');
            text.push('<div style="width: 24px; height: 12px; background-color: ' + chart.data.datasets[1].backgroundColor + ' "></div>');
            text.push('<p class="pt-2 m-0">Purchases</p>')
            text.push('</div>');
            text.push("</div>")
            return text.join('');
        },
        scales: {
            xAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    min: 2,
                    stepSize: 2
                }

            }],
            yAxes: [{
                display:false,
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
document.querySelector('#saleschart-legend').innerHTML = saleschart.generateLegend();
}

if ($("#saleschart-dark").length) {
    var saleschartcanvas = document.getElementById("saleschart-dark");
    var saleschart = new Chart(saleschartcanvas, {
        type: "line",
        data: {
            labels: [1,2,3,4,5,6,7,8,9,10,11,12,13],
            datasets: [{
                data: [10,5,8,6,3,5,7,8,3,6,5,8,4,6,7],
                backgroundColor: "#1ad5c3",
                pointRadius: 0,
                borderWidth: 0
                },
                {
                data: [12,14,15,13,17,19,16,14,13,12,16,17,14,12,14,16,18],
                backgroundColor: "#6201ed",
                pointRadius: 0,
                borderWidth: 0
                }
            ]
        },
        options: {
            legend: {
                display: false,
            },
            legendCallback : function(chart) {
                var text = [];
                text.push('<div class="d-flex align-items-center">');
                text.push('<div class="d-flex flex-column">');
                text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[0].data[0]+ '</h4 >');
                text.push('<div style="width: 24px; height: 12px; background-color: ' + chart.data.datasets[0].backgroundColor + ' "></div>');
                text.push('<p class="pt-2 m-0">Sales</p>')
                text.push('</div>');
                text.push('<div class="d-flex flex-column pl-4">');
                text.push('<h4 class="font-weight-bold">'+ chart.data.datasets[1].data[0]+ '</h4 >');
                text.push('<div style="width: 24px; height: 12px; background-color: ' + chart.data.datasets[1].backgroundColor + ' "></div>');
                text.push('<p class="pt-2 m-0">Purchases</p>')
                text.push('</div>');
                text.push("</div>")
                return text.join('');
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 2,
                        stepSize: 2
                    }
    
                }],
                yAxes: [{
                    display:false,
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
    document.querySelector('#saleschart-legend').innerHTML = saleschart.generateLegend();
    }

if ($("#purchaseschart").length) {
var purchaseschartcanvas = document.getElementById("purchaseschart");
var purchaseschart = new Chart(purchaseschartcanvas, {
    type: 'bar',
    data: {
        labels: ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'],
        datasets: [{
            data: [3.8,4.2,9,15,12.5,8,6,3,2,1.8,4.3,2],
            backgroundColor: "#fbbc06"
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines:{
                    display: false,
                    drawBorder: false
                },
                ticks: {
                    padding: 6,
                    fontColor: "#9b9b9b",
                    fontSize: 11
                }
                
            }],
            yAxes: [{
                ticks: {
                    min: 0,
                    stepSize: 4,
                    display: false,
                    
                },
                gridLines: {
                    drawBorder: false
                }
            }]
        }
    }

});
}

if ($("#purchaseschart-dark").length) {
    var purchaseschartcanvas = document.getElementById("purchaseschart-dark");
    var purchaseschart = new Chart(purchaseschartcanvas, {
        type: 'bar',
        data: {
            labels: ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'],
            datasets: [{
                data: [3.8,4.2,9,15,12.5,8,6,3,2,1.8,4.3,2],
                backgroundColor: "#fbbc06"
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines:{
                        display: false,
                        drawBorder: false,
                        color: "rgba(47, 50, 78, 0.4)"
                    },
                    ticks: {
                        padding: 6,
                        fontColor: "#9b9b9b",
                        fontSize: 11,
                        fontColor: "#d7d8db"
                    }
                    
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        stepSize: 4,
                        display: false,
                        fontColor: "#d7d8db"
                        
                    },
                    gridLines: {
                        drawBorder: false,
                        color: "rgba(47, 50, 78, 0.4)"
                    }
                }]
            }
        }
    
    });
    }

})(jQuery);